## Setup
- `$ sudo gem install homesick`
- `$ homesick clone https://gitlab.com/kaiyote/dotfiles`
- `$ homesick link dotfiles`

This depends on `zsh` and `starship` to function.
So, in order for this to function:
1. Make sure you're using `zsh` first.
2. Install [starship](https://starship.rs/guide/#%F0%9F%9A%80-installation)

Should also install [FuraCode Nerd Font](https://github.com/ryanoasis/nerd-fonts)
