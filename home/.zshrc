ANTIGEN_PATH=$HOME/.antigen
source $ANTIGEN_PATH/antigen.zsh

antigen init $HOME/.antigenrc

[ -d $HOME/.asdf/plugins/elixir ] || asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir
[ -d $HOME/.asdf/plugins/erlang ] || asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang
[ -d $HOME/.asdf/plugins/nodejs ] || asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs

[ -f $HOME/.zshrc.local ] && source $HOME/.zshrc.local

if ! starship_loc="$(type -p starship)" || [ -z $starship_loc ]; then
  echo "did not find starship."
  echo "please install starship as described here: https://starship.rs/guide/#%F0%9F%9A%80-installation"
else
  export STARSHIP_CONFIG=$HOME/.starship.toml
  eval "$(starship init zsh)"
fi
